package com.herokuapp.trytov.jarvis.features

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.herokuapp.trytov.jarvis.BaseException
import com.herokuapp.trytov.jarvis.R
import com.herokuapp.trytov.jarvis.SimpleSubscriber
import com.herokuapp.trytov.jarvis.data.Injection
import com.herokuapp.trytov.jarvis.exception.RestApiException
import com.herokuapp.trytov.jarvis.extensions.isInternetAvailable
import com.herokuapp.trytov.jarvis.extensions.showDialogOnlyAccept
import com.herokuapp.trytov.jarvis.features.home.HomeContract
import com.herokuapp.trytov.jarvis.features.home.HomeFragment
import com.herokuapp.trytov.jarvis.features.home.HomePresenter
import com.herokuapp.trytov.jarvis.util.TextReader
import com.herokuapp.trytov.jarvis.util.replaceFragmentInActivity


class MainActivity : AppCompatActivity(), HomePresenter.HomeCallBack {
    private lateinit var homePresenter: HomeContract.Presenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getResultAfterResolve()
        initSpeaker()
        directToHomePage()
        TextReader.initSpeaker(applicationContext)
        TextReader.getInstance()
    }

    override fun detectInternetState() {
        if (!this.isInternetAvailable()) {
            this.showDialogOnlyAccept(this, resources.getString(R.string.string_title_no_internet), resources.getString(R.string.string_content_no_internet))
        }
    }

    private fun directToHomePage() {
        HomeFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_layout_context)
            homePresenter = HomePresenter(it, this@MainActivity)
        }
    }

    private fun initSpeaker() {
        TextReader.initSpeaker(applicationContext)
        TextReader.getInstance()
    }

    private fun getResultAfterResolve() {
        val observer = object : SimpleSubscriber<String>() {
            override fun onCompleted(success: Boolean, value: String?, error: BaseException?) {
                super.onCompleted(success, value, error)
                when {
                    success && value != null -> showToast(value)
                    else -> handleExceptionOnRestApi(error!! as RestApiException)
                }
            }
        }
        Injection.providePackageRequestRepository().startServer().subscribe(observer)
    }

    private fun handleExceptionOnRestApi(error: RestApiException) {

        error.onError(object : RestApiException.CallBackException {
            override fun unAuthorized() {
            }

            override fun connectException() {
            }
        })
        when {
            !error.message.isNullOrEmpty() -> showToast(error.message!!)
            else -> false
        }
    }

    fun showToast(messege: String) {
        Toast.makeText(this, messege, Toast.LENGTH_SHORT).show()
    }
}
